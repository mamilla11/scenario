import sys
import json
import tasks
import psutil
import modulesApi as m
from ui import tao
from datetime import datetime
from PyQt5 import QtCore, QtGui, QtWidgets
import app_editor

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

ICON = 'ico/tao.png'
TEST_SCENARIO_1_PATH = 'test/типовой день города.json'
TEST_SCENARIO_2_PATH = 'test/тестовый сценарий.json'
COLOR_BACKGROUND = QtGui.QColor(57, 57, 57)
COLOR_BG_LIGHT_1 = QtGui.QColor(0, 137, 126)
COLOR_BG_LIGHT_2 = QtGui.QColor(0, 217, 162)
COLOR_BG_DARK_1 = QtGui.QColor(22, 95, 93)
COLOR_BG_DARK_2 = QtGui.QColor(21, 132, 108)
COLOR_BG_DARK_GRAY = QtGui.QColor(151, 175, 176)
COLOR_BG_LIGHT_GRAY = QtGui.QColor(212, 221, 224)

class Button(QtWidgets.QPushButton):
  def __init__(self, width, height, iconEnter, iconLeave, parent=None):
    super().__init__(parent)
    self.width = width
    self.height = height
    self.qss = 'border-style: solid; border-radius:3px;'

    self.setFixedSize(self.width, self.height)
    self.active = False
    self.enabled = True

    self.iconEnter = QtGui.QIcon()
    self.iconEnter.addPixmap(QtGui.QPixmap(iconEnter), QtGui.QIcon.Normal, QtGui.QIcon.On)

    self.iconLeave = QtGui.QIcon()
    self.iconLeave.addPixmap(QtGui.QPixmap(iconLeave), QtGui.QIcon.Normal, QtGui.QIcon.On)

    self.animationEnter = QtCore.QVariantAnimation(
      self, valueChanged=self.animateEnter,
      startValue=0.00001, endValue=0.9999, duration=200)

    self.animationLeave = QtCore.QVariantAnimation(
      self, valueChanged=self.animateLeave,
      startValue=0.00001, endValue=0.9999, duration=200)

  def applyActive(self):
    if self.active:
      self.animationEnter.setDirection(QtCore.QAbstractAnimation.Forward)
      self.animationEnter.start()
    else:
      self.animationLeave.setDirection(QtCore.QAbstractAnimation.Backward)
      self.animationLeave.start()

  def setActive(self, active):
    if self.active != active:
      self.active = active
      self.applyActive()

  def setEnabled(self, enabled):
    if self.enabled != enabled:
      self.enabled = enabled
      if self.enabled:
        self.applyActive()
      else:
        self.setStyleSheet(self.qss + 'background-color: {};'.format(COLOR_BG_DARK_GRAY.name()))

  def animateEnter(self, value):
    grad = "background-color: qlineargradient(spread:pad, x1:0, y1:0.5, x2:0, y2:0, stop:0 {color1}, stop: 1.0 {color2});".format(
      color1=COLOR_BG_DARK_1.name(), color2=COLOR_BG_DARK_2.name()
    )
    qss = self.qss + grad
    self.setStyleSheet(qss)
    self.setIcon(self.iconEnter)
    self.setIconSize(QtCore.QSize(self.width, self.height))
      
  def animateLeave(self, value):
    grad = "background-color: qlineargradient(spread:pad, x1:0, y1:0.8, x2:0, y2:0, stop:0 {color1}, stop:{value} {color1}, stop: 1.0 {color2});".format(
      color1=COLOR_BG_LIGHT_1.name(), color2=COLOR_BG_LIGHT_2.name(), value=value
    )
    qss = self.qss + grad
    self.setStyleSheet(qss)
    self.setIcon(self.iconLeave)
    self.setIconSize(QtCore.QSize(self.width, self.height))
      
  def enterEvent(self, event):
    if not self.active and self.enabled:
      self.animationEnter.setDirection(QtCore.QAbstractAnimation.Forward)
      self.animationEnter.start()
    super().enterEvent(event)

  def leaveEvent(self, event):
    if not self.active and self.enabled:
      self.animationLeave.setDirection(QtCore.QAbstractAnimation.Backward)
      self.animationLeave.start()
    super().leaveEvent(event)

  
class App(QtWidgets.QMainWindow, tao.Ui_MainWindow):
  def __init__(self):
    super(App, self).__init__()
    self.setupUi(self)
    self.setStyleSheet('background-color: {0};'.format(COLOR_BACKGROUND.name()))
    self.scenarioPath = ''
    self.scenarioName = ''
    self.scenario = {}
    self.isPaused = False
    self.isStarted = False
    self.isStopped = False
    self.isLooped = False
    self.editor = None
    self.threadpool = QtCore.QThreadPool()
    self.outputText.setReadOnly(True)
    self.outputText.setFont(self.getFont())
    self.outputText.setStyleSheet('color: {0}; background-color: {1}; border: none; border-radius: 3px'.format(COLOR_BACKGROUND.name(), COLOR_BG_LIGHT_GRAY.name()))
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(ICON), QtGui.QIcon.Normal, QtGui.QIcon.On)
    self.setWindowIcon(icon)
    
    self.moduleOfflineStyle = """
      border-radius:3px; 
      background-color: {color1};
      color: {color3}""".format(
        color1=COLOR_BG_DARK_GRAY.name(),
        color3=COLOR_BACKGROUND.name())

    self.moduleOnlineStyle = """
      border-radius:3px; 
      background-color: qlineargradient(spread:pad, x1:0, y1:0.8, x2:0, y2:0, stop:0 {color1}, stop: 1.0 {color2});
      color: {color3}""".format(
        color1=COLOR_BG_LIGHT_1.name(),
        color2=COLOR_BG_LIGHT_2.name(),
        color3=COLOR_BACKGROUND.name())

    self.pauseButton = Button(150, 150, 'ico/pause-on.png', 'ico/pause-off.png')
    self.pauseButton.clicked.connect(self.onPauseButtonClicked)
    self.pauseLayout.addWidget(self.pauseButton)

    self.playButton = Button(150, 150, 'ico/play-on.png', 'ico/play-off.png')
    self.playButton.clicked.connect(self.onPlayButtonClicked)
    self.playLayout.addWidget(self.playButton)

    self.repeatButton = Button(150, 150, 'ico/repeat-on.png', 'ico/repeat-off.png')
    self.repeatButton.clicked.connect(self.onRepeatButtonClicked)
    self.repeatLayout.addWidget(self.repeatButton)

    self.stopButton = Button(150, 150, 'ico/stop-on.png', 'ico/stop-off.png')
    self.stopButton.clicked.connect(self.onStopButtonClicked)
    self.stopLayout.addWidget(self.stopButton)

    self.openButton = Button(239, 59, 'ico/open-on.png', 'ico/open-off.png')
    self.openButton.clicked.connect(self.onOpenButtonClicked)
    self.openLayout.addWidget(self.openButton)

    self.editorButton = Button(239, 59, 'ico/editor-on.png', 'ico/editor-off.png')
    self.editorButton.clicked.connect(self.onOpenEditorClicked)
    self.editorLayout.addWidget(self.editorButton)

    self.testButton1 = Button(349, 59, 'ico/test1-on.png', 'ico/test1-off.png')
    self.testButton1.clicked.connect(self.onTestButton1Clicked)
    self.testLayout1.addWidget(self.testButton1)

    self.testButton2 = Button(349, 59, 'ico/test2-on.png', 'ico/test2-off.png')
    self.testButton2.clicked.connect(self.onTestButton2Clicked)
    self.testLayout2.addWidget(self.testButton2)

    m.loadModules()

    for module in m.modules:
      self.setModuleOfflineStyle(module.name)
    self.show()

  def onTestButton1Clicked(self):
    if not self.testButton1.enabled:
      return
    self.scenario = m.loadScenario(TEST_SCENARIO_1_PATH)
    result = m.verifyScenario(self.scenario)
    if (result == ''):
      self.scenarioPath = TEST_SCENARIO_1_PATH
      self.setScenarioName()
      self.outputText.clear()
    else:
      self.scenario = {}
      self.outputText.setText('Сценарий поврежден!\n{}'.format(result))

  def onTestButton2Clicked(self):
    if not self.testButton2.enabled:
      return
    self.scenario = m.loadScenario(TEST_SCENARIO_2_PATH)
    result = m.verifyScenario(self.scenario)
    if (result == ''):
      self.scenarioPath = TEST_SCENARIO_2_PATH
      self.setScenarioName()
      self.outputText.clear()
    else:
      self.scenario = {}
      self.outputText.setText('Сценарий поврежден!\n{}'.format(result))

  def onOpenEditorClicked(self):
    if not self.editorButton.enabled:
      return
    if not self.editor:
      self.playerEnable(False)
      self.editorButton.setActive(True)
      self.editor = app_editor.App(self.scenarioPath)      
      self.editor.signals.closed.connect(self.onEditorClosed)
    else:
      self.outputText.setText('Редактор сценариев уже запущен!')

  def onEditorClosed(self):
    if self.scenarioPath:
      self.scenario = m.loadScenario (self.scenarioPath)
      result = m.verifyScenario(self.scenario)
      if (result != ''):
        self.scenario = {}
        self.outputText.setText('Сценарий поврежден!\n{}'.format(result))
    self.playerEnable(True)
    self.editorButton.setActive(False)
    self.editor = None

  def onOpenButtonClicked(self):
    if not self.openButton.enabled:
      return
    dialog = QtWidgets.QFileDialog()
    dialog.setDefaultSuffix('json')
    dialog.setNameFilters(['JSON (*.json)'])
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
      self.scenario = m.loadScenario (dialog.selectedFiles()[0])
      result = m.verifyScenario(self.scenario)
      if (result == ''):
        self.scenarioPath = dialog.selectedFiles()[0]
        self.setScenarioName()
        self.outputText.clear()
      else:
        self.scenario = {}
        self.outputText.setText('Сценарий поврежден!\n{}'.format(result))


  def onPlayButtonClicked(self):
    if (not self.editor):
      if self.scenarioPath:
        self.isPaused = False
        self.pauseButton.setActive(False)
        self.stopButton.setActive(False)
        self.playButton.setActive(True)
        self.editorButton.setEnabled(False)
        self.testButton1.setEnabled(False)
        self.testButton2.setEnabled(False)
        self.openButton.setEnabled(False)
        self.start()
      else:
        self.outputText.setText('Сценарий не выбран!')
    else:
      self.outputText.setText('Закройте редактор сценариев')

  def onPauseButtonClicked(self):
    if (not self.editor):
      if self.scenarioPath and not self.isPaused and self.threadpool.activeThreadCount():
        self.isPaused = True
        self.pauseButton.setActive(True)
        self.stopButton.setActive(False)
        self.playButton.setActive(False)
    else:
      self.outputText.setText('Закройте редактор сценариев')

  def onStopButtonClicked(self):
    if (not self.editor):
      if self.scenarioPath and self.isStarted:
        self.isStarted = False
        self.pauseButton.setActive(False)
        self.playButton.setActive(False)
        self.isStopped = True
    else:
      self.outputText.setText('Закройте редактор сценариев')

  def onRepeatButtonClicked(self):
    if (not self.editor):
      self.isLooped = not self.isLooped
      if self.isLooped:
        self.repeatButton.setActive(True)
      else:
        self.repeatButton.setActive(False)
    else:
      self.outputText.setText('Закройте редактор сценариев')

  def onStepFinished(self, stepNumber, status, desc):
    self.outputText.append('[{0}] Шаг {1} >>> {2}'.format(datetime.now().strftime("%H:%M:%S"), stepNumber, status))
    if (desc):
      self.outputText.append('{0}\n'.format(desc))
    self.highlightOnlineModules()

  def onOnlineModulesUpdated(self):
    self.highlightOnlineModules()
  
  def onFinished(self):
    self.isStarted = False
    self.isStopped = False
    self.isPaused = False
    self.outputText.append('[{0}] Сценарий завершен!\n'.format(datetime.now().strftime("%H:%M:%S")))
    self.playButton.setActive(False)
    self.editorButton.setEnabled(True)
    self.testButton1.setEnabled(True)
    self.testButton2.setEnabled(True)
    self.openButton.setEnabled(True)

    for module in m.modules:
      module.online = False
      self.setModuleOfflineStyle(module.name)
  
  def getFont(self):
    font = QtGui.QFont()
    font.setFamily("Arial")
    font.setPointSize(20)
    return font

  def start(self):
    if not self.isStarted:
      self.isStarted = True
      self.outputText.clear()
      self.outputText.append('[{0}] Сценарий запущен\n'.format(datetime.now().strftime("%H:%M:%S")))
      self.outputText.append('[{0}] Определение модулей в сети...\n'.format(datetime.now().strftime("%H:%M:%S")))
      task = tasks.Player(self)
      task.signals.stepFinished.connect(self.onStepFinished)
      task.signals.onlineModulesUpdated.connect(self.onOnlineModulesUpdated)
      task.signals.finished.connect(self.onFinished)
      self.threadpool.start(task)

  def setScenarioName(self):
    name = (self.scenarioPath.split('/')[-1]).split('.')[0]
    self.nameLabel.setStyleSheet('color: white')
    self.nameLabel.setFont(self.getFont())
    self.nameLabel.setText(name.upper())

  def getFont(self):
    font = QtGui.QFont()
    font.setFamily("Arial")
    font.setWeight(70)
    font.setPointSize(20)
    return font

  def highlightOnlineModules(self):
    for module in m.modules:
      if module.online:
        self.setModuleOnlineStyle(module.name)
      else:
        self.setModuleOfflineStyle(module.name)

  def setModuleOnlineStyle(self, name):
    list(filter(lambda x: x.text() == name, self.modulesFrame.children()))[0].setStyleSheet(self.moduleOnlineStyle)

  def setModuleOfflineStyle(self, name):
    list(filter(lambda x: x.text() == name, self.modulesFrame.children()))[0].setStyleSheet(self.moduleOfflineStyle)

  def playerEnable(self, enable):
    self.playButton.setEnabled(enable)
    self.pauseButton.setEnabled(enable)
    self.stopButton.setEnabled(enable)
    self.repeatButton.setEnabled(enable)

  def closeEvent(self, event):
    if self.threadpool.activeThreadCount():
      msgBox = QtWidgets.QMessageBox(self)
      msgBox.setIcon(QtWidgets.QMessageBox.Warning)
      msgBox.setWindowTitle(self.windowTitle())
      msgBox.setText('Остановите запущенный сценарий.')
      msgBox.setFont(self.getFont())
      msgBox.setStyleSheet('color: {0};'.format(COLOR_BG_LIGHT_GRAY.name()))
      msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
      msgBox.exec_()
      event.ignore()
      return
    if self.editor:
      self.editor.close()

def main():
  app = QtWidgets.QApplication(sys.argv) 

  icon = QtGui.QIcon()
  icon.addPixmap(QtGui.QPixmap(ICON), QtGui.QIcon.Normal, QtGui.QIcon.On)

  msgBox = QtWidgets.QMessageBox()
  msgBox.setIcon(QtWidgets.QMessageBox.Warning)
  msgBox.setWindowTitle('Внимание')
  msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
  msgBox.setWindowIcon(icon)

  if ('TaoManual.exe' in (p.name() for p in psutil.process_iter())):
    msgBox.setText('Закройте Ручное управление (TaoManual.exe) и запустите приложение заново.')
    msgBox.exec_()
    sys.exit()

  if ('TaoEditor.exe' in (p.name() for p in psutil.process_iter())):
    msgBox.setText('Закройте Редактор сценариев (TaoEditor.exe) и запустите приложение заново.')
    msgBox.exec_()
    sys.exit()

  data = list(filter(lambda x: x.name() == 'Tao.exe', psutil.process_iter()))
  if (len(data) > 2):
    msgBox.setText('Приложение уже запущено.')
    msgBox.exec_()
    sys.exit()

  window = App()  
  ret = app.exec_()
  sys.exit(ret)

if __name__ == '__main__':
  main()
