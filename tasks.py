import time
import modulesApi as m
from datatypes.railway import Railway
from datetime import datetime
from PyQt5 import QtCore

PORT = 502

class UpdateSignals(QtCore.QObject):
  updated = QtCore.pyqtSignal()
  railwayUpdated = QtCore.pyqtSignal(int, int, int, int, int, int)

class Updater(QtCore.QRunnable):
  MODULES_UPDATE_MS = 1000

  def __init__(self):
    super(Updater, self).__init__()
    self.signals = UpdateSignals()
    self.railway = Railway()
    self.timeMs = lambda: int(round(time.time() * 1000))
    self.lastUpdateMs = self.timeMs() - Updater.MODULES_UPDATE_MS

  @QtCore.pyqtSlot()
  def run(self):    
    while True:
      if (self.timeMs() - self.lastUpdateMs) > Updater.MODULES_UPDATE_MS:
        # Обновляем все модули
        for module in m.modules:
          module.online = module.updateRegisters()
        # Обновляем состояние железной дороги
        self.railway.process()
        # Обновляем время последнего обновления
        self.lastUpdateMs = self.timeMs()
        # Сообщаем в родительский процесс, что модули обновлены
        self.signals.updated.emit()

class PlayerSignals(QtCore.QObject):
  stepFinished = QtCore.pyqtSignal(str, str, str)
  onlineModulesUpdated = QtCore.pyqtSignal()
  finished = QtCore.pyqtSignal()

class Player(QtCore.QRunnable):
  MODULES_UPDATE_MS = 1000

  def __init__(self, player):
    super(Player, self).__init__()
    self.signals = PlayerSignals()
    self.player = player
    self.timeMs = lambda: int(round(time.time() * 1000))
    self.stepDurationMs = self.player.scenario['duration'] * 1000
    self.lastStepMs = self.timeMs() - self.stepDurationMs
    self.lastUpdateMs = self.timeMs() - Player.MODULES_UPDATE_MS
    self.railway = Railway()

  # Обновление состояний объектов.
  def nextStep(self, stepIndex):
    # Потом обновляем все модули
    for inst in self.player.scenario['scenario']:
      module = m.getModuleById(inst["module"])
      for item in inst['items']:
        module.setItemState(item['id'], item['steps'][stepIndex])
      for i in range(3):
        module.online = module.updateRegisters()
        if module.online:
          break

  # Проверка модулей в сети.
  def checkOnline(self):
    for module in m.modules:
      module.isOnline()
    self.signals.onlineModulesUpdated.emit()

  @QtCore.pyqtSlot()
  def run(self):
    currentStep = 0
    timePassed = 0

    # Проверка модулей в сети.
    self.checkOnline()

    while True:
      # Если сценарий остановлен, возвращаем объекты в 
      # исходное состояние.
      if self.player.isStopped:
        currentStep = 0
        self.nextStep(currentStep)
        self.signals.finished.emit()
        break

      # Если сценарий поставлен на паузу, остаемся на текущем шаге сценария.
      # Обновляем self.lastStepMs, чтобы начать с того же момента, когда
      # сценарий был поставлен на паузу.
      if self.player.isPaused:
        self.lastStepMs = self.timeMs() - timePassed
      else:
        timePassed = self.timeMs() - self.lastStepMs
      
      # Если пришло время, устанавливаем следующий шаг сценария.
      if timePassed > self.stepDurationMs:
        # Обновляем время перехода к шагу сценария
        self.lastStepMs = self.timeMs()

        # Если все шаги сценария пройдены, то либо начинаем заново
        # (если включена опция зацикливания сценария),
        # либо возвращаем все объекты в исходное состояние и завершаем сценарий.
        if (currentStep >= self.player.scenario['steps']):
          currentStep = 0
          if not self.player.isLooped:
            self.nextStep(currentStep)
            self.signals.finished.emit()
            break
        
        # Устанавливаем следующий шаг сценария.
        self.nextStep(currentStep)

        self.signals.stepFinished.emit(
          '{0}/{1}'.format(currentStep + 1, self.player.scenario['steps']),
          'OK', self.player.scenario['descs'][currentStep])
        currentStep += 1
        
      # Oбновляем состояние железной дороги
      if (self.timeMs() - self.lastUpdateMs) > Player.MODULES_UPDATE_MS:
        self.railway.process()
        self.lastUpdateMs = self.timeMs()
