import sys
import json
import tasks
import modulesApi as m
from datetime import datetime
from ui import playerui
from PyQt5 import QtCore, QtGui, QtWidgets

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

class PlayerDialog(QtWidgets.QDialog, playerui.Ui_Dialog):
  def __init__(self, parent, scenarioPath, modules):
    super(PlayerDialog, self).__init__(parent)
    self.setupUi(self)

    self.scenario = {}

    self.isPaused = False
    self.isStarted = False
    self.isStopped = False
    self.isLooped = False

    self.modules = modules
    self.setup(scenarioPath)

    self.threadpool = QtCore.QThreadPool()

    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("ico/play-off.png"), QtGui.QIcon.Normal, QtGui.QIcon.On)
    self.playButton.setIcon(icon)
    self.playButton.setIconSize(QtCore.QSize(30, 30))

    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("ico/pause-off.png"), QtGui.QIcon.Normal, QtGui.QIcon.On)
    self.pauseButton.setIcon(icon)
    self.pauseButton.setIconSize(QtCore.QSize(30, 30))

    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("ico/stop-off.png"), QtGui.QIcon.Normal, QtGui.QIcon.On)
    self.stopButton.setIcon(icon)
    self.stopButton.setIconSize(QtCore.QSize(30, 30))

    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("ico/repeat-off.png"), QtGui.QIcon.Normal, QtGui.QIcon.On)
    self.repeatButton.setIcon(icon)
    self.repeatButton.setIconSize(QtCore.QSize(30, 30))

    self.playButton.clicked.connect(self.onPlayButtonClicked)
    self.pauseButton.clicked.connect(self.onPauseButtonClicked)
    self.stopButton.clicked.connect(self.onStopButtonClicked)
    self.repeatButton.clicked.connect(self.onRepeatButtonClicked)
    self.updateButton.clicked.connect(self.onUpdateButtonClicked)

    self.pauseButton.setEnabled(False)
    self.stopButton.setEnabled(False)
    self.outputText.setReadOnly(True)

  def setup(self, scenarioPath):
    with open(scenarioPath, 'r', encoding='utf-8') as rfile:
      self.scenario = json.load(rfile)

  def highlightOnlineModules(self):
    for module in self.modules:
      if module.online:
        self.setModuleOnlineStyle(module.name)
      else:
        self.setModuleOfflineStyle(module.name)

  def setModuleOnlineStyle(self, name):
    list(filter(lambda x: x.text() == name, self.modulesFrame.children()))[0].setStyleSheet("background-color: yellowgreen")

  def setModuleOfflineStyle(self, name):
    list(filter(lambda x: x.text() == name, self.modulesFrame.children()))[0].setStyleSheet("background-color: none")

  def start(self):  
    if not self.isStarted:
      self.isStarted = True
      self.outputText.setText('Сценарий запущен...\n')
      task = tasks.Player(self)
      task.signals.stepFinished.connect(self.onStepFinished)
      task.signals.onlineModulesUpdated.connect(self.onOnlineModulesUpdated)
      task.signals.finished.connect(self.onFinished)
      self.threadpool.start(task)

  def closeEvent(self, event):
    if self.threadpool.activeThreadCount():
      msgBox = QtWidgets.QMessageBox(self)
      msgBox.setIcon(QtWidgets.QMessageBox.Warning)
      msgBox.setWindowTitle(self.windowTitle())
      msgBox.setText('Остановите запущенный сценарий.')
      msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
      msgBox.exec_()
      event.ignore()
      return
      
  def onStepFinished(self, stepNumber, status, desc):
    self.outputText.append('[{0}] шаг {1} >>> {2}'.format(datetime.now().strftime("%H:%M:%S"), stepNumber, status))
    if (desc):
      self.outputText.append('{0}\n'.format(desc))
    self.highlightOnlineModules()

  def onOnlineModulesUpdated(self):
    self.highlightOnlineModules()
  
  def onFinished(self):
    self.isStarted = False
    self.isStopped = False
    self.updateButton.setEnabled(True)
    self.pauseButton.setEnabled(False)
    self.stopButton.setEnabled(False)
    self.playButton.setEnabled(True)
    self.outputText.append('Сценарий закончен!\n')

  def onPlayButtonClicked(self):
    self.updateButton.setEnabled(False)
    self.pauseButton.setEnabled(True)
    self.stopButton.setEnabled(True)
    self.playButton.setEnabled(False)
    self.isPaused = False
    self.start()

  def onPauseButtonClicked(self):
    self.updateButton.setEnabled(True)
    self.pauseButton.setEnabled(False)
    self.stopButton.setEnabled(False)
    self.playButton.setEnabled(True)
    self.isPaused = True
    self.outputText.append('Сценарий поставлен на паузу!\n')

  def onStopButtonClicked(self):
    self.updateButton.setEnabled(True)
    self.pauseButton.setEnabled(False)
    self.stopButton.setEnabled(False)
    self.playButton.setEnabled(True)
    self.isStarted = False
    self.isStopped = True
    self.outputText.append('Сценарий остановлен!\n')

  def onRepeatButtonClicked(self):
    self.isLooped = not self.isLooped
    if self.isLooped:
      self.outputText.append('Сценарий зациклен.\n')
    else:
      self.outputText.append('Сценарий проиграется один раз.\n')

  def onUpdateButtonClicked(self):
    self.highlightOnlineModules()
