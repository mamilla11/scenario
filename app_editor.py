import sys
import os
import json
import psutil
import modulesApi as m
from ui import scenario
from PyQt5 import QtCore, QtGui, QtWidgets
from datetime import datetime
from playerDialog import PlayerDialog

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

ICON = 'ico/editor.png'
TEXT_OK   = 'норма'
TEXT_OK_REV   = 'норма (Р)'
TEXT_WARN = 'не норма'
TEXT_WARN_REV = 'не норма (Р)'
TEXT_ERR  = 'авария'
TEXT_OFF  = 'выключено'

STATE_LIST = [TEXT_OK, TEXT_OK_REV, TEXT_WARN, TEXT_WARN_REV, TEXT_ERR, TEXT_OFF]

COLOR_OK   = QtGui.QColor(218, 247, 166)
COLOR_WARN = QtGui.QColor(247, 220, 111)
COLOR_ERR  = QtGui.QColor(241, 148, 138)
COLOR_OFF  = QtGui.QColor(234, 237, 237)

VERTICAL_HEADER_OFFSET = QtCore.QPoint(360, 20)

SORT_TYPES = ['по типу', 'по модулю', 'по алфавиту']
SCALE_VARIANTS = ['1:1', '1:2', '1:3', '1:5', '1:6', '1:10', '1:15', '1:20', '1:30', '1:60']
START_TIMES = ['{0:02}:00'.format(i) for i in range(0, 24)]

class AppSignals(QtCore.QObject):
  closed = QtCore.pyqtSignal()

class App(QtWidgets.QMainWindow, scenario.Ui_MainWindow, QtCore.QRunnable):
  def __init__(self, scenarioPath=None):
    super(App, self).__init__()
    self.setupUi(self)
    self.items = []
    self.settings = self.getSettings()
    self.scale = 1
    self.startTime = 0
    self.stepsCount = 0
    self.itemsCount = 0
    self.scenarioName = ''
    self.scenarioPath = ''
    self.descs = []
    self.signals = AppSignals()
    self.setWindowIcon(self.getEditorIcon())
    self.popMenu = self.getPopMenu()
    self.setBlockColorLegend()
    self.sortCombo.addItems(SORT_TYPES)
    self.scaleCombo.addItems(SCALE_VARIANTS)
    self.startTimeCombo.addItems(START_TIMES)

    self.tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
    self.tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Fixed)
    self.tableWidget.verticalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Fixed)
    self.tableWidget.itemSelectionChanged.connect(self.onTableSelection)
    self.tableWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
    self.tableWidget.customContextMenuRequested.connect(self.onContextMenuRequest)

    self.delStepButton.clicked.connect(self.onDelStepClicked)
    self.addStepButton.clicked.connect(self.onAddStepClicked)
    self.stepTimeSpin.valueChanged.connect(self.onStepTimeChanged)
    self.sortCombo.currentIndexChanged.connect(self.onSortTypeChanged)
    self.popMenu.aboutToShow.connect(self.onPopMenuAboutToShow)
    self.popMenu.triggered[QtWidgets.QAction].connect(self.onPopMenuSelectionChanged)
    self.scaleCombo.currentIndexChanged.connect(self.onScaleChanged)
    self.startTimeCombo.currentIndexChanged.connect(self.onStartTimeChanged)
    self.descText.textChanged.connect(self.onDescriptionChanged)

    self.createButton.setShortcut("Ctrl+N")
    self.createButton.clicked.connect(self.onCreateScenarioClicked)
    self.saveButton.setShortcut("Ctrl+S")
    self.saveButton.clicked.connect(self.onSaveScenarioClicked)
    self.playButton.setShortcut("Ctrl+E")
    self.playButton.clicked.connect(self.onPlayScenario)
    self.openButton.setShortcut("Ctrl+O")
    self.openButton.clicked.connect(self.onOpenScenarioClicked)
    self.saveAsButton.clicked.connect(self.onSaveAsClicked)

    self.disableControls()
    self.updateDescFrame()

    if scenarioPath:
      self.scenarioPath = scenarioPath
      self.scenarioName = scenarioPath.split('/')[-1]
      self.openScenario()

    self.show()


  def getSettings(self):
    data = None
    with open('data/settings.json', 'r', encoding='utf-8') as rfile:
      data = json.load(rfile)
    return data

  #######################################################
  # Returns editor icon element
  #######################################################
  def getEditorIcon(self):
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(ICON), QtGui.QIcon.Normal, QtGui.QIcon.On)
    return icon


  #######################################################
  # Returns popup menu element
  #######################################################
  def getPopMenu(self):
    popMenu = QtWidgets.QMenu(self)
    font = QtGui.QFont()
    font.setFamily("Consolas")
    font.setPointSize(20)
    popMenu.setFont(font)
    return popMenu


  #######################################################
  # Loads modules data
  #######################################################
  def loadModulesData(self):
    m.loadModules()
    self.items = m.getItems()


  #######################################################
  # Clears scenario
  #######################################################
  def clearScenario(self):
    self.tableWidget.clear()
    self.items.clear()
    self.delSteps(self.stepsCount)

    self.addStepSpin.setValue(0)
    self.delStepSpin.setValue(0)
    self.sortCombo.setCurrentIndex(0)
    self.scaleCombo.setCurrentIndex(0)
    self.startTimeCombo.setCurrentIndex(0)
    self.stepTimeSpin.setValue(1)
      

  #######################################################
  # Renders scenario
  #######################################################
  def renderScenario(self):
    self.stepsCount = len(self.items[0].scenario)
    self.itemsCount = len(self.items)

    self.tableWidget.setRowCount(self.itemsCount)

    for i in range(0, self.stepsCount):
      self.setTableHorizontalHeader(i)
    
    self.fillTable()
    self.calcTotalTime()
    self.onSortTypeChanged(0)
    self.enableControls()
    self.scaleCombo.setCurrentIndex(SCALE_VARIANTS.index('1:{}'.format(self.scale)))
    self.startTimeCombo.setCurrentIndex(START_TIMES.index('{0:02}:00'.format(int(self.startTime / 3600))))


  #######################################################
  # Creates scenario
  #######################################################
  def onCreateScenarioClicked(self):
    if self.hasUnsavedChanges() and (not self.handleUnsavedChanges(discardAction=self.openScenario)):
      return

    self.setWindowTitle('Редактор сценариев - [Новый сценарий] *')
    self.clearScenario()
    self.loadModulesData()
    self.renderScenario()
    self.descs = ['']
    self.updateDescFrame()
    self.scenarioPath = ''
    self.scenarioName = ''


  #######################################################
  # Saves scenario to the current scenario path.
  #######################################################
  def onSaveScenarioClicked(self):
    if self.hasUnsavedChanges():
      if not self.scenarioPath:
        if not self.selectFileToSave():
          return
      self.saveScenario()
      self.setWindowTitle('Редактор сценариев - [{}]'.format(self.scenarioName))


  #######################################################
  # Saves scenario to the specified scenario path.
  #######################################################
  def onSaveAsClicked(self):
    if self.hasUnsavedChanges():
      if not self.selectFileToSave():
        return
      self.saveScenario()
      self.setWindowTitle('Редактор сценариев - [{}]'.format(self.scenarioName))


  #######################################################
  # Shows dialog to select file to save.
  #######################################################
  def selectFileToSave(self):
    dialog = QtWidgets.QFileDialog()
    dialog.setDefaultSuffix('json')
    dialog.setNameFilters(['JSON (*.json)'])
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
      path = dialog.selectedFiles()[0]
      name = path.split('/')[-1]
      if os.path.exists(path):
        result = self.rewriteScenarioDialog(name)
        if (result == QtWidgets.QMessageBox.Save):
          self.scenarioPath = path
          self.scenarioName = name
        else:
          return False
      else:
        self.scenarioPath = path
        self.scenarioName = name
    else: 
      return False
    return True


  #######################################################
  # Saves scenario.
  #######################################################
  def saveScenario(self):
    itemsJson = {}
    itemsJson['steps'] = self.stepsCount
    itemsJson['duration'] = self.stepTimeSpin.value()
    itemsJson['scale'] = self.scale
    itemsJson['start'] = self.startTime
    itemsJson['descs'] = self.descs
    itemsJson['scenario'] = []

    itemsCopy = sorted(self.items, key=lambda x: x.id)
    currentModuleId = 0
    moduleIndex = -1
    for item in itemsCopy:
      if currentModuleId != item.moduleId:
        currentModuleId = item.moduleId
        itemsJson['scenario'].append({'module': currentModuleId, 'items': []})
        moduleIndex += 1
      itemsJson['scenario'][moduleIndex]['items'].append({"id": item.id, "steps": item.scenario})
    with open(self.scenarioPath, 'w', encoding='utf-8') as wfile:
      wfile.write(json.dumps(itemsJson))


  #######################################################
  # Opens existing scenario
  #######################################################
  def openScenario(self):
    if not self.scenarioPath:
      return

    data = m.loadScenario(self.scenarioPath)
    result = m.verifyScenario(data)
    if result != '':
      return result

    scenario = data['scenario']

    self.clearScenario()
    self.stepTimeSpin.setValue(data['duration'])
    self.descs = data['descs']
    self.scale = data['scale']
    self.startTime = data['start']
    self.setWindowTitle('Редактор сценариев - [{0}]'.format(self.scenarioName))
    self.loadModulesData()

    for module in scenario:
      for item in module['items']:
        m.getItemById(item['id']).scenario = item['steps']
  
    for item in self.items:
      if len(item.scenario) != data['steps']:
        item.scenario = [0 for x in range (0, data['steps'])]

    self.renderScenario()
    return result
    

  #######################################################
  # Disables controls
  #######################################################
  def disableControls(self):
    self.addStepSpin.setEnabled(False)
    self.delStepSpin.setEnabled(False)
    self.addStepButton.setEnabled(False)
    self.delStepButton.setEnabled(False)
    self.sortCombo.setEnabled(False)
    self.scaleCombo.setEnabled(False)
    self.startTimeCombo.setEnabled(False)
    self.stepTimeSpin.setEnabled(False)


  #######################################################
  # Enables controls
  #######################################################
  def enableControls(self):
    self.addStepSpin.setEnabled(True)
    self.delStepSpin.setEnabled(True)
    self.addStepButton.setEnabled(True)
    self.delStepButton.setEnabled(True)
    self.sortCombo.setEnabled(True)
    self.scaleCombo.setEnabled(True)
    self.startTimeCombo.setEnabled(True)
    self.stepTimeSpin.setEnabled(True)


  #######################################################
  # Adds a star if scenario is changed
  #######################################################
  def markAsChanged(self):
    if '*' not in self.windowTitle():
      self.setWindowTitle('{} *'.format(self.windowTitle()))


  #######################################################
  # Returns True is there are unsaved changes
  #######################################################
  def hasUnsavedChanges(self):
    return '*' in self.windowTitle()


  #######################################################
  # Saves/discards unsaved changes on app close event
  #######################################################
  def closeEvent(self, event):
    if self.hasUnsavedChanges():
      if not self.handleUnsavedChanges():
        event.ignore()
        return
    self.signals.closed.emit()


  #######################################################
  # Handles unsaved changes. Returns True on success
  #######################################################
  def handleUnsavedChanges(self, discardAction=None):
    result = self.showSaveChangesDialog()
    if result == QtWidgets.QMessageBox.Save:
      self.saveScenario()
      if self.scenarioPath:
        return True
      return False
    if result == QtWidgets.QMessageBox.Discard:
      if discardAction:
        discardAction()
      return True
    if result == QtWidgets.QMessageBox.Cancel:
      return False


  #######################################################
  # Shows dialog to rewrite existing scenario
  #######################################################
  def rewriteScenarioDialog(self, filename):
    msgBox = QtWidgets.QMessageBox(self)
    msgBox.setWindowIcon(self.getEditorIcon())
    msgBox.setIcon(QtWidgets.QMessageBox.Question)
    msgBox.setWindowTitle('Редактор сценариев')
    msgBox.setText('Сценарий {0} уже существует.'.format(filename))
    msgBox.setInformativeText("Заменить его?")
    msgBox.setStandardButtons(QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Cancel)
    msgBox.setDefaultButton(QtWidgets.QMessageBox.Save)
    return msgBox.exec_()

  #######################################################
  # Shows dialog to discard or save unsaved changes
  #######################################################
  def showSaveChangesDialog(self):
    msgBox = QtWidgets.QMessageBox(self)
    msgBox.setWindowIcon(self.getEditorIcon())
    msgBox.setIcon(QtWidgets.QMessageBox.Question)
    msgBox.setWindowTitle('Редактор сценариев')
    msgBox.setText('Имеются не сохраненные изменения в сценарии.')
    msgBox.setInformativeText("Сохранить изменения?")
    msgBox.setStandardButtons(QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Discard | QtWidgets.QMessageBox.Cancel)
    msgBox.setDefaultButton(QtWidgets.QMessageBox.Save)
    return msgBox.exec_()


  #######################################################
  # Fills current step description window
  #######################################################
  def updateDescFrame(self):
    if (self.tableWidget.currentColumn() == -1):
      self.descLabel.setText('Описание шага')
      self.descText.setEnabled(False)
      self.descText.setText('')
    else:
      self.descLabel.setText('Описание шага №{0}'.format(self.tableWidget.currentColumn() + 1))
      self.descText.setEnabled(True)
      self.descText.setText(self.descs[self.tableWidget.currentColumn()])
      
  
  #######################################################
  # Fills table with data
  #######################################################
  def fillTable(self):
    self.tableWidget.clearContents()

    itemIndex = 0
    for item in self.items:
      self.setTableVerticalHeader(itemIndex, item)
      self.setSteps(itemIndex, item)
      itemIndex += 1


  #######################################################
  # Fills row of a table
  #######################################################
  def setSteps(self, itemIndex, item):
    stepIndex = 0
    for step in item.scenario:
      tableCell = QtWidgets.QTableWidgetItem()
      tableCell.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
      self.changeCellColor(tableCell, item.states[step])
      self.tableWidget.setItem(itemIndex, stepIndex, tableCell)
      stepIndex += 1


  #######################################################
  # Adds steps to the table
  #######################################################
  def addSteps(self, stepsCount):
    startIndex = self.stepsCount
    itemIndex = 0
    stepsState = [0 for i in range(0, stepsCount)]

    for step in range(0, stepsCount):
      self.setTableHorizontalHeader(startIndex + step)

    for item in self.items:
      item.scenario.extend(stepsState)
      for step in range(0, stepsCount):
        tableCell = QtWidgets.QTableWidgetItem()
        tableCell.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self.changeCellColor(tableCell, TEXT_OFF)
        self.tableWidget.setItem(itemIndex, (startIndex + step), tableCell)
      itemIndex += 1

    self.stepsCount += stepsCount
    self.descs.extend('' for x in range(0, stepsCount))
    self.updateDescFrame()


  #######################################################
  # Deletes steps from the table
  #######################################################
  def delSteps(self, stepsCount):
    startIndex = self.stepsCount - 1
    for step in range(0, stepsCount):
      if (startIndex >= 0):
        self.tableWidget.removeColumn(startIndex)
        self.descs.pop()
        startIndex -= 1

    for item in self.items:
      for step in range(0, stepsCount):
        if (len(item.scenario) != 0):
          item.scenario.pop()

    self.stepsCount -= stepsCount
    if (self.stepsCount < 0):
      self.stepsCount = 0

    self.tableWidget.setColumnCount(self.stepsCount)
    self.calcTotalTime()
    self.markAsChanged()
    self.updateDescFrame()


  #######################################################
  # Setups vertical table header
  #######################################################
  def setTableVerticalHeader(self, itemIndex, item):
    block = item.block
    
    moduleName = m.getModuleById(item.moduleId).name
    rowLabelText = '[{0}] {1}'.format(moduleName.ljust(3), item.name)

    font = QtGui.QFont("Consolas")
    font.setWeight(75)

    rowLabel = QtWidgets.QTableWidgetItem(QtGui.QIcon(self.settings['blocks'][block]['icon']), rowLabelText)
    rowLabel.setFont(font)

    self.tableWidget.setVerticalHeaderItem(itemIndex, rowLabel)


  #######################################################
  # Setups horizontal table header
  #######################################################
  def setTableHorizontalHeader(self, index):
    self.tableWidget.insertColumn(index)
    self.tableWidget.setColumnWidth(index, 80)
    self.updateTableHorizontalHeader(index)
    

  #######################################################
  # Updates table header with appropriate timestamp
  #######################################################
  def updateTableHorizontalHeader(self, index):
    timeInSec = self.startTime + index * self.stepTimeSpin.value() * self.scale

    while (timeInSec > 86400):
      timeInSec -= 86400

    if (timeInSec != 0):
      m, s = divmod(timeInSec, 60) 
      h, m = divmod(m, 60) 
    else:
      h, m, s = 0, 0, 0

    if (h == 24):
      h = 0

    colLabelText = 'Шаг {0}\r\n{1:02}:{2:02}:{3:02}'.format(index + 1, h, m, s)
    colLabel = QtWidgets.QTableWidgetItem(colLabelText)
    self.tableWidget.setHorizontalHeaderItem(index, colLabel)


  #######################################################
  # Setups legend
  #######################################################
  def setBlockColorLegend(self):
    model = QtGui.QStandardItemModel(self.legendTable)

    for block in self.settings['blocks']:
      item = QtGui.QStandardItem(self.settings['blocks'][block]['name'])
      item.setIcon(QtGui.QIcon(self.settings['blocks'][block]['icon']))
      model.appendRow(item)
    
    self.legendTable.setModel(model)


  #######################################################
  # Sets cell color depending on state
  #######################################################
  def changeCellColor(self, item, state):
    if state == TEXT_OK or state == TEXT_OK_REV:
      item.setBackground(COLOR_OK)
    elif state == TEXT_WARN or state == TEXT_WARN_REV:
      item.setBackground(COLOR_WARN)
    elif state == TEXT_ERR:
      item.setBackground(COLOR_ERR)
    else:
      item.setBackground(COLOR_OFF)

    item.setText(state)
    self.tableWidget.clearSelection()


  #######################################################
  # Calculates total scenario time
  #######################################################
  def calcTotalTime(self):
    stepTime = self.stepTimeSpin.value()
    timeInSec = stepTime * self.stepsCount

    if (timeInSec != 0):
      m, s = divmod(timeInSec, 60) 
      h, m = divmod(m, 60) 
    else:
      h, m, s = 0, 0, 0

    self.totalTimeLabel.setText('{0:02}ч. {1:02}м. {2:02}сек.'.format(h, m, s))


  #######################################################
  # Opens dialog to select scenario to open
  #######################################################
  def onOpenScenarioClicked(self):
    if self.hasUnsavedChanges() and (not self.handleUnsavedChanges(discardAction=self.openScenario)):
      return
      
    dialog = QtWidgets.QFileDialog()
    dialog.setDefaultSuffix('json')
    dialog.setNameFilters(['JSON (*.json)'])
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
      prevScenarioPath = self.scenarioPath
      prevScenarioName = self.scenarioName
      self.scenarioPath = dialog.selectedFiles()[0]
      self.scenarioName = self.scenarioPath.split('/')[-1]
      result = self.openScenario()
      if result != '':
        self.scenarioPath = prevScenarioPath
        self.scenarioName = prevScenarioName
        msgBox = QtWidgets.QMessageBox(self)
        msgBox.setIcon(QtWidgets.QMessageBox.Critical)
        msgBox.setWindowTitle('Редактор сценариев')
        msgBox.setText('Сценарий поврежден.')
        msgBox.setInformativeText(result)
        msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msgBox.exec_()


  #######################################################
  # Opens player to execute scenario
  #######################################################
  def onPlayScenario(self):
    if (self.scenarioPath == "") or (self.scenarioPath == None):
      msgBox = QtWidgets.QMessageBox(self)
      msgBox.setWindowIcon(self.getEditorIcon())
      msgBox.setIcon(QtWidgets.QMessageBox.Critical)
      msgBox.setWindowTitle('Редактор сценариев')
      msgBox.setText('Сценарий не выбран.')
      msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
      msgBox.setDefaultButton(QtWidgets.QMessageBox.Ok)
      msgBox.exec_()
      return

    if self.hasUnsavedChanges() and (not self.handleUnsavedChanges(discardAction=self.openScenario)):
      return

    dialog = PlayerDialog(self, self.scenarioPath, m.modules)
    dialog.exec_()

  #######################################################
  # Setups popup menu with actions applicable to all items
  #######################################################
  def onPopMenuAboutToShow(self):
    self.popMenu.clear()

    items = set()
    for item in self.tableWidget.selectedItems():
      items.add(item.row())

    states = set(STATE_LIST)
    for item in items:
      states = states.intersection(set(self.items[item].states))
    
    for state in self.sortPopMenuStates(states):
      self.popMenu.addAction(QtWidgets.QAction(state, self))
  

  #######################################################
  # Sorts available steps for popup menu
  #######################################################
  def sortPopMenuStates(self, states):
    sortedStates = []
    for state in STATE_LIST:
      if state in states:
        sortedStates.append(state)
    return sortedStates


  #######################################################
  # Changes step state for all selected items
  #######################################################
  def onPopMenuSelectionChanged(self, selection):
    for selectedItem in self.tableWidget.selectedItems():
      item = selectedItem.row()
      step = selectedItem.column()
      state = self.items[item].states.index(selection.text())
      self.items[item].scenario[step] = state
      self.changeCellColor(selectedItem, selection.text())
    self.markAsChanged()


  #######################################################
  # Table cells selected event handler
  #######################################################
  def onTableSelection(self):
    self.updateDescFrame()
    if (len(self.tableWidget.selectedItems())) == 0:
      self.tableWidget.clearSelection()


  #######################################################
  # Shows context menu
  #######################################################
  def onContextMenuRequest(self, point):
    point += VERTICAL_HEADER_OFFSET
    self.popMenu.exec_(self.tableWidget.mapToGlobal(point))
  

  #######################################################
  # Deletes specified number of steps
  #######################################################
  def onDelStepClicked(self):
    steps2del = self.delStepSpin.value()
    if steps2del:
      self.delSteps(steps2del)


  #######################################################
  # Adds specified number of steps
  #######################################################
  def onAddStepClicked(self):
    steps2add = self.addStepSpin.value()
    if steps2add:
      self.addSteps(steps2add)
      self.tableWidget.setColumnCount(self.stepsCount)
      self.calcTotalTime()
      self.markAsChanged()
  

  #######################################################
  # Recalculates all times according to new step time
  #######################################################
  def onStepTimeChanged(self):
    self.calcTotalTime()
    for index in range(0, self.stepsCount):
      self.updateTableHorizontalHeader(index)
      self.markAsChanged()


  #######################################################
  # Recalculates all times according to new time scale
  #######################################################
  def onScaleChanged(self, index):
    self.scale = int(SCALE_VARIANTS[index].split(':')[1])
    for index in range(0, self.stepsCount):
      self.updateTableHorizontalHeader(index)
  

  #######################################################
  # Recalculates all times according to new start time
  #######################################################
  def onStartTimeChanged(self, index):
    self.startTime = int(START_TIMES[index].split(':')[0]) * 3600
    for index in range(0, self.stepsCount):
      self.updateTableHorizontalHeader(index)


  #######################################################
  # Assigns/updates step description
  #######################################################
  def onDescriptionChanged(self):
    if (self.tableWidget.currentColumn() != -1):
      line = self.descText.toPlainText()
      if (line != self.descs[self.tableWidget.currentColumn()]):
        self.descs[self.tableWidget.currentColumn()] = line
        self.markAsChanged()
  
  
  #######################################################
  # Sorts all items according to selected sort type
  #######################################################
  def onSortTypeChanged(self, index):
    if (index == 0):
      self.items = sorted(self.items, key=lambda x: x.name.lower())
      self.items = sorted(self.items, key=lambda x: x.block)
    if (index == 1):
      self.items = sorted(self.items, key=lambda x: x.name.lower())
      self.items = sorted(self.items, key=lambda x: x.id)
    if (index == 2):
      self.items = sorted(self.items, key=lambda x: x.name.lower())
    
    self.fillTable()


def main():
  app = QtWidgets.QApplication(sys.argv)

  icon = QtGui.QIcon()
  icon.addPixmap(QtGui.QPixmap(ICON), QtGui.QIcon.Normal, QtGui.QIcon.On)

  msgBox = QtWidgets.QMessageBox()
  msgBox.setIcon(QtWidgets.QMessageBox.Warning)
  msgBox.setWindowTitle('Внимание')
  msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
  msgBox.setWindowIcon(icon)

  if ('TaoManual.exe' in (p.name() for p in psutil.process_iter())):
    msgBox.setText('Закройте Ручное управление (TaoManual.exe) и запустите приложение заново.')
    msgBox.exec_()
    sys.exit()

  if ('Tao.exe' in (p.name() for p in psutil.process_iter())):
    msgBox.setText('Закройте Пульт управления (Tao.exe) и запустите приложение заново.')
    msgBox.exec_()
    sys.exit()

  data = list(filter(lambda x: x.name() == 'TaoEditor.exe', psutil.process_iter()))
  if (len(data) > 2):
    msgBox.setText('Приложение уже запущено.')
    msgBox.exec_()
    sys.exit()

  if len(sys.argv) == 2:
    window = App(sys.argv[1])  
  else:
    window = App()

  ret = app.exec_()
  sys.exit(ret)

if __name__ == '__main__':
  main()