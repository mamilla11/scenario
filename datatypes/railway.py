import modulesApi as m

M10_REG_GATE14_15 = 19
M10_REG_GATE17b = 20
M10_REG_GATE18 = 21

M14_REG_GATE = 18
M15_REG_GATE = 19
M17b_REG_GATE = 12
M18_REG_GATE = 18

class Railway():
  def __init__(self):
    self.module10 = m.getModuleById(10)
    self.module14 = m.getModuleById(14)
    self.module15 = m.getModuleById(15)
    self.module17b = m.getModuleById(117)
    self.module18 = m.getModuleById(18)
    self.gate14_15 = None
    self.gate17b = None
    self.gate18 = None

  #######################################################
  # Контролирует работу железной дороги.
  #######################################################
  def process(self):
    if self.module10.online and self.module14.online and self.module15.online:
      # Читаем, в каком состоянии должны быть шлагбаумы на модуле 14 и 15
      gate14_15 = self.module10.readRegister(M10_REG_GATE14_15)
      if gate14_15 != None:
        # Записываем состояние шлагбаумов в модуль 14 и 15
        self.module14.writeRegister(M14_REG_GATE, gate14_15)
        self.module15.writeRegister(M15_REG_GATE, gate14_15)
        if self.gate14_15 != gate14_15:
          # Обновляем локальное состояние шлагбаума 14 и 15
          self.gate14_15 = gate14_15
          print ('шлагбаум 14, 15: {}'.format(self.gate14_15))

    if self.module10.online and self.module17b.online:
      # Читаем, в каком состоянии должны быть шлагбаумы на модуле 17б
      gate17b = self.module10.readRegister(M10_REG_GATE17b)
      if gate17b != None:
        # Записываем состояние шлагбаумов в модуль 17б
        self.module17b.writeRegister(M17b_REG_GATE, gate17b)
        if self.gate17b != gate17b:
          # Обновляем локальное состояние шлагбаума 17б
          self.gate17b = gate17b
          print ('шлагбаум 17b: {}'.format(self.gate17b))

    if self.module10.online and self.module18.online:
      # Читаем, в каком состоянии должны быть шлагбаумы на модуле 18
      gate18 = self.module10.readRegister(M10_REG_GATE18)
      if gate18 != None:
        # Записываем состояние шлагбаумов в модуль 18
        self.module18.writeRegister(M18_REG_GATE, gate18)
        if self.gate18 != gate18:
          # Обновляем локальное состояние шлагбаума 18
          self.gate18 = gate18
          print ('шлагбаум 18: {}'.format(self.gate18))
