from datatypes.register import Register

class Item():
  def __init__(self, data, moduleId):
    self.moduleId = moduleId
    self.id = data['id']
    self.name = data['name']
    self.block = data['block']
    self.states = data['states']
    self.scenario = [0]
    self.state = 0
    self.registers = []
    for reg in data['registers']:
      self.registers.append(Register(reg, self.id, self.name))
  
  #######################################################
  # Возвращает список текущих значений всех регистов объекта.
  #######################################################
  def getRegisterValues(self):
    values = []
    for reg in self.registers:
      values.append(reg.value)
    return values

  #######################################################
  # Задает для всех регистров объекта значение, 
  # соответствующее сценарному состоянию.
  #######################################################
  def setScenarioState(self, state):
    if len(self.states) > state:
      self.state = state
    else:
      self.state = 0
    for reg in self.registers:
      reg.setScenarioValue(self.state)

  #######################################################
  # Возвращает название текущего сценарного состояния.
  #######################################################
  def state2string(self):
    return self.states[self.state]