import time
from datatypes.item import Item
from pyModbusTCP.client import ModbusClient

HEARTBEAT_ADDRESS = 0
PORT = 502

class Module():
  def __init__(self, data):
    self.id = data['id']
    self.name = data['name']
    self.ip = data['ip']
    self.online = False
    self.registers = []
    self.items = []
    self.rx_error = 0
    self.tx_error = 0

    for i in range(0, len(data['items'])):
      self.items.append(Item(data['items'][i], self.id))
      self.registers.extend(self.items[i].registers)

  #######################################################
  # Проверяет, находится ли модуль в сети.
  #######################################################
  def isOnline(self):
    self.online = self.readRegister(HEARTBEAT_ADDRESS) != None
    return self.online

  #######################################################
  # Задает сценарное состояние для соответствующего объекта.
  # itemId - id объекта.
  # state - сценарное состояние.
  #######################################################
  def setItemState(self, itemId, state):
    foundItems = list(filter(lambda x: x.id == itemId, self.items))
    if len(foundItems) == 1:
      foundItems[0].setScenarioState(state)
    elif len(foundItems) > 1:
      print('[модуль {0}] несколько объектов с одинаковым id={1}'.format(self.name, itemId))
    else:
      print('[модуль {0}] объекта с id={1} не обнаружено'.format(self.name, itemId))

  #######################################################
  # Читает регистр по адресу.
  # address - адрес регистра.
  #######################################################
  def readRegister(self, address):
    c = ModbusClient(host=self.ip, port=PORT, timeout=0.05, auto_open=True, auto_close=True)
    result = c.read_holding_registers(address)
    if result:
      return result[0]
    self.rx_error += 1
    return None

  #######################################################
  # Читает регистры по адресу.
  # address - адрес первого регистра.
  #######################################################
  def readRegisters(self, address, count):
    c = ModbusClient(host=self.ip, port=PORT, timeout=0.05, auto_open=True, auto_close=True)
    result = c.read_holding_registers(address, count)
    if result:
      return result
    self.rx_error += 1
    return None

  #######################################################
  # Записывает регистр по адресу.
  # address - адрес регистра.
  # value - значение регистра.
  #######################################################
  def writeRegister(self, address, value):
    c = ModbusClient(host=self.ip, port=PORT, timeout=0.05, auto_open=True, auto_close=True)
    if not c.write_single_register(address, value):
      self.tx_error += 1
      return False
    return True

  #######################################################
  # Записывает несколько регистров по адресу.
  # address - адрес первого регистра.
  # values - значения.
  #######################################################
  def writeRegisters(self, address, values):
    c = ModbusClient(host=self.ip, port=PORT, timeout=0.05, auto_open=True, auto_close=True)
    if not c.write_multiple_registers(address, values):
      self.tx_error += 1
      return False
    return True
      
  #######################################################
  # Обновляет все регистры текущими значениями.
  #######################################################
  def updateRegisters(self):
    values = []
    for register in self.registers:
      values.append(register.value)
    c = ModbusClient(host=self.ip, port=PORT, timeout=0.1, auto_open=True, auto_close=True)
    if not c.write_multiple_registers(1, values):
      self.tx_error += 1
      return False
    return True
