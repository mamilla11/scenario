import json
from datatypes.module import Module

MODULES_COUNT = 29

FILE_NAMES = [
  'module_1.json',
  'module_2.json',
  'module_2b.json',
  'module_3.json',
  'module_4.json',
  'module_5.json',
  'module_5b.json',
  'module_6.json',
  'module_7.json',
  'module_8.json',
  'module_9.json',
  'module_10.json',
  'module_11.json',
  'module_12.json',
  'module_12b.json',
  'module_13.json',
  'module_14.json',
  'module_15.json',
  'module_16.json',
  'module_17.json',
  'module_17b.json',
  'module_18.json',
  'module_19.json',
  'module_20.json',
  'module_20b.json',
  'module_21.json',
  'module_22.json',
  'module_23.json',
  'module_24.json',
]

modules = None

#######################################################
# Loads modules data
#######################################################
def loadModules():
  global modules

  modules = []
  for i in range(0, MODULES_COUNT):
    modules.append(Module(getModuleData(FILE_NAMES[i])['module']))

def getItems():
  global modules
  items = []
  for module in modules:
    items.extend(module.items)
  return items

def getModuleById(id):
  global modules
  foundModules = list(filter(lambda x: x.id == id, modules))
  if len(foundModules) == 1:
    return foundModules[0]
  return None

def getItemById(id):
  foundItems = list(filter(lambda x: x.id == id, getItems()))
  if len(foundItems) == 1:
    return foundItems[0]
  return None
  
def getModuleData(fileName):
  data = None
  with open('data/{}'.format(fileName), 'r', encoding='utf-8') as rfile:
    data = json.load(rfile)
  return data

def loadScenario(filePath):
  data = None
  try:
    with open(filePath, 'r', encoding='utf-8') as rfile:
      data = json.load(rfile)
  except:
    print('Файл не соответствует формату JSON')
  return data

def verifyScenario(data):
  loadModules()
  items_ids = set()
  items_cnt = 0
  if data == None:
    return 'Файл не соответствует формату JSON'
  if (not 'steps' in data.keys()) or (type(data['steps']) != int):
    return 'Неверный формат поля steps.'
  if (not 'duration' in data.keys()) or (type(data['duration']) != int):
    return 'Неверный формат поля duration.'
  if (not 'scale' in data.keys()) or (type(data['scale']) != int):
    return 'Неверный формат поля scale.'
  if (not 'start' in data.keys()) or (type(data['start']) != int):
    return 'Неверный формат поля start.'
  if (not 'descs' in data.keys()) or (type(data['descs']) != list):
    return 'Неверный формат поля descs.'
  if (not 'scenario' in data.keys()) or (type(data['scenario']) != list):
    return 'Неверный формат поля scenario.'
  if (len(data['scenario']) != MODULES_COUNT):
    return 'В сценарии описано неверное количество модулей.'

  for module in data['scenario']:
    if (type(module) != dict):
     return 'Неверный формат элемента scenario.'
    if (not 'module' in module.keys()) or (type(module['module']) != int):
      return 'Неверный формат поля module.'
    if (getModuleById (module['module']) == None):
      return 'Модуля с id={} не существует.'.format(module['module'])
    if (not 'items' in module.keys()) or (type(module['items']) != list):
      return 'Неверный формат поля items.'
    if (len(module['items']) == 0):
      return 'Неверный формат поля items.'

    for item in module['items']:
      if (type(item) != dict):
        return 'Неверный формат элемента items.'
      if (not 'id' in item.keys()) or (type(item['id']) != int):
        return 'Неверный формат поля id.'
      if (getItemById (item['id']) == None):
        return 'Объекта с id={} не существует.'.format(item['id'])
      items_ids.add(item['id'])
      items_cnt += 1
      if (not 'steps' in item.keys()) or (type(item['steps']) != list):
        return 'Неверный формат поля steps.'
      if (len(item['steps']) != data['steps']):
        return 'Количество сценарных шагов объекта не соответствует значению поля steps.'

  if (len(items_ids) != items_cnt):
    return 'ID объектов в сценарии не должны пересекаться.'
  return ''
  

