import sys
import tasks
import psutil
import modulesApi as m
from datatypes.register import HW_TYPE_VALUES
from datetime import datetime
from ui import manualui
from PyQt5 import QtCore, QtGui, QtWidgets

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

ICON = 'ico/manual.png'
COLOR_ONLINE = QtGui.QColor(218, 247, 166)
COLOR_OFFLINE = QtGui.QColor(234, 237, 237)

class RegisterView():
  def __init__(self, register, parent):
    self.register = register
    self.parent = parent
    self.view = None
    self.widget = None
    self.setupWidget()
    self.setupView()

  def setupWidget(self):
    self.widget = QtWidgets.QComboBox()
    self.widget.setFixedWidth(220)
    self.widget.wheelEvent = lambda event: None
    self.widget.addItems(HW_TYPE_VALUES[self.register.typeHw])
    self.widget.currentIndexChanged.connect(lambda index: self.onStateChanged(index))

  def setupView(self):
    self.view = QtWidgets.QTreeWidgetItem(self.parent)
    self.view.setText(0, self.register.name)
    self.view.setText(1, str(self.register.address))
    self.view.setText(2, self.register.typeHw)
    self.view.setText(4, self.register.itemName)

  def onStateChanged(self, index):
    self.register.setValueByIndex(index)

  def setState(self, strValue):
    self.register.setValueByName(strValue)
    self.widget.setCurrentIndex(self.register.value)

class ModuleView():
  def __init__(self, module, parent):
    self.module = module
    self.parent = parent
    self.view = None
    self.registerViews = []
    self.render()
  
  def render(self):
    self.view = QtWidgets.QTreeWidgetItem(self.parent)
    self.view.setFont(0, QtGui.QFont("Consolas", weight=QtGui.QFont.Bold))
    self.view.setText(0, "{0} Модуль {1}".format(self.isOnlineText(), self.module.name))
    self.view.setFlags(self.view.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)

    for register in self.module.registers:
      registerView = RegisterView(register, self.view)
      self.parent.setItemWidget(registerView.view, 3, registerView.widget)
      self.registerViews.append(registerView)

  def isOnlineText(self):
    if self.module.online:
      return 'Онлайн'
    return 'Офлайн'

  def setColor(self, color):
    self.view.setBackground(0, color)
    self.view.setBackground(1, color)
    self.view.setBackground(2, color)
    self.view.setBackground(3, color)
    self.view.setBackground(4, color)

  def setText(self):
    if self.module.online:
      self.setColor(COLOR_ONLINE)
    else:
      self.setColor(COLOR_OFFLINE)
    self.view.setText(0, "{0} Модуль {1}".format(self.isOnlineText(), self.module.name))

  def setStateForRegisters(self, strTypeHw, strState):
    for view in self.registerViews:
      if view.register.typeHw == strTypeHw:
        view.setState(strState)

class App(QtWidgets.QMainWindow, manualui.Ui_MainWindow):
  def __init__(self):
    super(App, self).__init__()
    self.setupUi(self)
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(ICON), QtGui.QIcon.Normal, QtGui.QIcon.On)
    self.setWindowIcon(icon)
    self.modules = []

    self.treeWidget.headerItem().setText(0, "Интерактивность")
    self.treeWidget.headerItem().setText(1, "Адрес")
    self.treeWidget.headerItem().setText(2, "Тип")
    self.treeWidget.headerItem().setText(3, "Состояние")
    self.treeWidget.headerItem().setText(4, "Объект")
    self.treeWidget.setColumnWidth(0, 240)
    self.treeWidget.setColumnWidth(1, 50)
    self.treeWidget.setColumnWidth(2, 170)
    self.treeWidget.setColumnWidth(3, 240)

    self.cb1.addItems(HW_TYPE_VALUES[self.l1.text()])
    self.cb2.addItems(HW_TYPE_VALUES[self.l2.text()])
    self.cb3.addItems(HW_TYPE_VALUES[self.l3.text()])
    self.cb4.addItems(HW_TYPE_VALUES[self.l4.text()])
    self.cb5.addItems(HW_TYPE_VALUES[self.l5.text()])
    self.cb6.addItems(HW_TYPE_VALUES[self.l6.text()])
    self.cb7.addItems(HW_TYPE_VALUES[self.l7.text()])
    self.cb8.addItems(HW_TYPE_VALUES[self.l8.text()])
    self.cb9.addItems(HW_TYPE_VALUES[self.l9.text()])
    self.cb10.addItems(HW_TYPE_VALUES[self.l10.text()])
    self.cb11.addItems(HW_TYPE_VALUES[self.l11.text()])
    self.cb12.addItems(HW_TYPE_VALUES[self.l12.text()])
    self.cb13.addItems(HW_TYPE_VALUES[self.l13.text()])
    self.cb14.addItems(HW_TYPE_VALUES[self.l14.text()])
    self.cb15.addItems(HW_TYPE_VALUES[self.l15.text()])
    self.cb16.addItems(HW_TYPE_VALUES[self.l16.text()])

    self.cb1.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l1.text(), str(self.cb1.currentText())))
    self.cb2.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l2.text(), str(self.cb2.currentText())))
    self.cb3.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l3.text(), str(self.cb3.currentText())))
    self.cb4.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l4.text(), str(self.cb4.currentText())))
    self.cb5.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l5.text(), str(self.cb5.currentText())))
    self.cb6.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l6.text(), str(self.cb6.currentText())))
    self.cb7.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l7.text(), str(self.cb7.currentText())))
    self.cb8.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l8.text(), str(self.cb8.currentText())))
    self.cb9.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l9.text(), str(self.cb9.currentText())))
    self.cb10.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l10.text(), str(self.cb10.currentText())))
    self.cb11.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l11.text(), str(self.cb11.currentText())))
    self.cb12.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l12.text(), str(self.cb12.currentText())))
    self.cb13.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l13.text(), str(self.cb13.currentText())))
    self.cb14.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l14.text(), str(self.cb14.currentText())))
    self.cb15.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l15.text(), str(self.cb15.currentText())))
    self.cb16.currentIndexChanged.connect(lambda:self.onButtonClicked(self.l16.text(), str(self.cb16.currentText())))
    
    m.loadModules()
    for module in m.modules:
      self.modules.append(ModuleView(module, self.treeWidget))

    task = tasks.Updater()
    task.signals.updated.connect(self.onUpdated)

    self.threadpool = QtCore.QThreadPool()
    self.threadpool.start(task)

    self.show()

  def onButtonClicked(self, strHwType, strState):
    for module in self.modules:
      module.setStateForRegisters(strHwType, strState)
  
  def onUpdated(self):
    for module in self.modules:
      module.setText()
    print ('[{0}] - last update'.format(datetime.now().strftime("%H:%M:%S")))

def main():
  app = QtWidgets.QApplication(sys.argv) 
  
  icon = QtGui.QIcon()
  icon.addPixmap(QtGui.QPixmap(ICON), QtGui.QIcon.Normal, QtGui.QIcon.On)

  msgBox = QtWidgets.QMessageBox()
  msgBox.setIcon(QtWidgets.QMessageBox.Warning)
  msgBox.setWindowTitle('Внимание')
  msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
  msgBox.setWindowIcon(icon)

  if ('TaoEditor.exe' in (p.name() for p in psutil.process_iter())):
    msgBox.setText('Закройте Редактор сценариев (TaoEditor.exe) и запустите приложение заново.')
    msgBox.exec_()
    sys.exit()

  if ('Tao.exe' in (p.name() for p in psutil.process_iter())):
    msgBox.setText('Закройте Пульт управления (Tao.exe) и запустите приложение заново.')
    msgBox.exec_()
    sys.exit()

  data = list(filter(lambda x: x.name() == 'TaoManual.exe', psutil.process_iter()))
  if (len(data) > 2):
    msgBox.setText('Приложение уже запущено.')
    msgBox.exec_()
    sys.exit()

  window = App()  
  app.exec_()

if __name__ == '__main__':
  main()