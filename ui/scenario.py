# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scenario.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1462, 698)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMinimumSize(QtCore.QSize(0, 678))
        self.centralwidget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.gridLayout.setObjectName("gridLayout")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setMinimumSize(QtCore.QSize(1400, 203))
        self.frame.setMaximumSize(QtCore.QSize(16777215, 203))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.controlFrame = QtWidgets.QFrame(self.frame)
        self.controlFrame.setMinimumSize(QtCore.QSize(155, 199))
        self.controlFrame.setMaximumSize(QtCore.QSize(155, 199))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        self.controlFrame.setFont(font)
        self.controlFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.controlFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.controlFrame.setObjectName("controlFrame")
        self.playButton = QtWidgets.QPushButton(self.controlFrame)
        self.playButton.setGeometry(QtCore.QRect(0, 160, 151, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.playButton.setFont(font)
        self.playButton.setObjectName("playButton")
        self.openButton = QtWidgets.QPushButton(self.controlFrame)
        self.openButton.setGeometry(QtCore.QRect(0, 120, 151, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.openButton.setFont(font)
        self.openButton.setObjectName("openButton")
        self.createButton = QtWidgets.QPushButton(self.controlFrame)
        self.createButton.setGeometry(QtCore.QRect(0, 0, 151, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.createButton.setFont(font)
        self.createButton.setObjectName("createButton")
        self.saveButton = QtWidgets.QPushButton(self.controlFrame)
        self.saveButton.setGeometry(QtCore.QRect(0, 40, 151, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.saveButton.setFont(font)
        self.saveButton.setObjectName("saveButton")
        self.saveAsButton = QtWidgets.QPushButton(self.controlFrame)
        self.saveAsButton.setGeometry(QtCore.QRect(0, 80, 151, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.saveAsButton.setFont(font)
        self.saveAsButton.setObjectName("saveAsButton")
        self.horizontalLayout.addWidget(self.controlFrame)
        self.legendTable = QtWidgets.QListView(self.frame)
        self.legendTable.setMinimumSize(QtCore.QSize(220, 199))
        self.legendTable.setMaximumSize(QtCore.QSize(220, 199))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        self.legendTable.setFont(font)
        self.legendTable.setObjectName("legendTable")
        self.horizontalLayout.addWidget(self.legendTable)
        self.setupFrame = QtWidgets.QFrame(self.frame)
        self.setupFrame.setMinimumSize(QtCore.QSize(429, 0))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        self.setupFrame.setFont(font)
        self.setupFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.setupFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setupFrame.setObjectName("setupFrame")
        self.delStepLabel = QtWidgets.QLabel(self.setupFrame)
        self.delStepLabel.setGeometry(QtCore.QRect(150, 60, 101, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.delStepLabel.setFont(font)
        self.delStepLabel.setObjectName("delStepLabel")
        self.line_2 = QtWidgets.QFrame(self.setupFrame)
        self.line_2.setGeometry(QtCore.QRect(129, 70, 21, 111))
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.sortCombo = QtWidgets.QComboBox(self.setupFrame)
        self.sortCombo.setGeometry(QtCore.QRect(184, 0, 227, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.sortCombo.setFont(font)
        self.sortCombo.setObjectName("sortCombo")
        self.addStepSpin = QtWidgets.QSpinBox(self.setupFrame)
        self.addStepSpin.setGeometry(QtCore.QRect(14, 100, 110, 38))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.addStepSpin.setFont(font)
        self.addStepSpin.setObjectName("addStepSpin")
        self.addStepButton = QtWidgets.QPushButton(self.setupFrame)
        self.addStepButton.setGeometry(QtCore.QRect(14, 140, 111, 38))
        self.addStepButton.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.addStepButton.setFont(font)
        self.addStepButton.setObjectName("addStepButton")
        self.addStepLabel = QtWidgets.QLabel(self.setupFrame)
        self.addStepLabel.setGeometry(QtCore.QRect(14, 60, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.addStepLabel.setFont(font)
        self.addStepLabel.setObjectName("addStepLabel")
        self.delStepSpin = QtWidgets.QSpinBox(self.setupFrame)
        self.delStepSpin.setGeometry(QtCore.QRect(150, 100, 110, 38))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.delStepSpin.setFont(font)
        self.delStepSpin.setObjectName("delStepSpin")
        self.delStepButton = QtWidgets.QPushButton(self.setupFrame)
        self.delStepButton.setGeometry(QtCore.QRect(150, 140, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.delStepButton.setFont(font)
        self.delStepButton.setObjectName("delStepButton")
        self.sortLabel = QtWidgets.QLabel(self.setupFrame)
        self.sortLabel.setGeometry(QtCore.QRect(14, 0, 161, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.sortLabel.setFont(font)
        self.sortLabel.setObjectName("sortLabel")
        self.horizontalLayout.addWidget(self.setupFrame)
        self.descFrame = QtWidgets.QVBoxLayout()
        self.descFrame.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.descFrame.setObjectName("descFrame")
        self.descLabel = QtWidgets.QLabel(self.frame)
        self.descLabel.setMinimumSize(QtCore.QSize(0, 38))
        self.descLabel.setMaximumSize(QtCore.QSize(16777215, 38))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(11)
        self.descLabel.setFont(font)
        self.descLabel.setObjectName("descLabel")
        self.descFrame.addWidget(self.descLabel)
        self.descText = QtWidgets.QTextEdit(self.frame)
        self.descText.setMinimumSize(QtCore.QSize(0, 143))
        self.descText.setMaximumSize(QtCore.QSize(16777215, 143))
        self.descText.setObjectName("descText")
        self.descFrame.addWidget(self.descText)
        self.horizontalLayout.addLayout(self.descFrame)
        self.timeFrame = QtWidgets.QFrame(self.frame)
        self.timeFrame.setMinimumSize(QtCore.QSize(380, 199))
        self.timeFrame.setMaximumSize(QtCore.QSize(380, 199))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        self.timeFrame.setFont(font)
        self.timeFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.timeFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.timeFrame.setObjectName("timeFrame")
        self.stepTimeSpin = QtWidgets.QSpinBox(self.timeFrame)
        self.stepTimeSpin.setGeometry(QtCore.QRect(290, 150, 90, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.stepTimeSpin.setFont(font)
        self.stepTimeSpin.setMinimum(1)
        self.stepTimeSpin.setMaximum(3600)
        self.stepTimeSpin.setObjectName("stepTimeSpin")
        self.startTimeLabel = QtWidgets.QLabel(self.timeFrame)
        self.startTimeLabel.setGeometry(QtCore.QRect(10, 50, 281, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.startTimeLabel.setFont(font)
        self.startTimeLabel.setWordWrap(True)
        self.startTimeLabel.setObjectName("startTimeLabel")
        self.stepTimeLabel = QtWidgets.QLabel(self.timeFrame)
        self.stepTimeLabel.setGeometry(QtCore.QRect(10, 150, 281, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.stepTimeLabel.setFont(font)
        self.stepTimeLabel.setWordWrap(True)
        self.stepTimeLabel.setObjectName("stepTimeLabel")
        self.totalTimeLabel = QtWidgets.QLabel(self.timeFrame)
        self.totalTimeLabel.setGeometry(QtCore.QRect(170, 0, 131, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setItalic(True)
        self.totalTimeLabel.setFont(font)
        self.totalTimeLabel.setObjectName("totalTimeLabel")
        self.label = QtWidgets.QLabel(self.timeFrame)
        self.label.setGeometry(QtCore.QRect(10, 0, 161, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setItalic(True)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.startTimeCombo = QtWidgets.QComboBox(self.timeFrame)
        self.startTimeCombo.setGeometry(QtCore.QRect(290, 50, 90, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.startTimeCombo.setFont(font)
        self.startTimeCombo.setObjectName("startTimeCombo")
        self.scaleLabel = QtWidgets.QLabel(self.timeFrame)
        self.scaleLabel.setGeometry(QtCore.QRect(10, 100, 281, 38))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.scaleLabel.setFont(font)
        self.scaleLabel.setObjectName("scaleLabel")
        self.scaleCombo = QtWidgets.QComboBox(self.timeFrame)
        self.scaleCombo.setGeometry(QtCore.QRect(290, 100, 91, 38))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.scaleCombo.setFont(font)
        self.scaleCombo.setObjectName("scaleCombo")
        self.horizontalLayout.addWidget(self.timeFrame)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.gridLayout.addWidget(self.tableWidget, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Редактор сценариев"))
        self.playButton.setText(_translate("MainWindow", "Запустить"))
        self.openButton.setText(_translate("MainWindow", "Открыть"))
        self.createButton.setText(_translate("MainWindow", "Создать"))
        self.saveButton.setText(_translate("MainWindow", "Сохранить"))
        self.saveAsButton.setText(_translate("MainWindow", "Сохранить как"))
        self.delStepLabel.setText(_translate("MainWindow", "Удалить шаги"))
        self.addStepButton.setText(_translate("MainWindow", "Применить"))
        self.addStepLabel.setText(_translate("MainWindow", "Добавить шаги"))
        self.delStepButton.setText(_translate("MainWindow", "Применить"))
        self.sortLabel.setText(_translate("MainWindow", "Сортировать объекты"))
        self.descLabel.setText(_translate("MainWindow", "Описание шага"))
        self.startTimeLabel.setText(_translate("MainWindow", "Время начала работы сценария (ч.):"))
        self.stepTimeLabel.setText(_translate("MainWindow", "Время экспозиции шага (сек.):"))
        self.totalTimeLabel.setText(_translate("MainWindow", "00ч. 00м. 00сек."))
        self.label.setText(_translate("MainWindow", "Общее время работы:"))
        self.scaleLabel.setText(_translate("MainWindow", "Масштаб времени:"))

