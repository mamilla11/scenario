# scenario

pyuic5 manual.ui -o manualui.py
pyuic5 scenario.ui -o scenario.py
pyuic5 player.ui -o playerui.py
pyuic5 tao.ui -o tao.py

pyinstaller -F --windowed -p ui -p datatypes -i ico/manual.ico -n TaoManual app_manual.py
pyinstaller -F --windowed -p ui -p datatypes -i ico/tao.ico -n Tao app.py
pyinstaller -F --windowed -p ui -p datatypes -i ico/editor.ico -n TaoEditor app_editor.py
